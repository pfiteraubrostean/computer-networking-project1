#!/usr/bin/env python3

"""HTTP server

This script is used to creat and start the HTTP server
"""

from argparse import ArgumentParser
from webhttp.server import Server


def run_server():
    """Create and start the HTTP server

    Use "python webserver.py --help" to display command line options
    """
    parser = ArgumentParser(description="HTTP Server")
    parser.add_argument("-a", "--address", type=str, default="localhost")
    parser.add_argument("-p", "--port", type=int, default=8001)
    parser.add_argument("-t", "--timeout", type=int, default=15)
    args = parser.parse_args()

    # Start server
    server = Server(args.address, args.port, args.timeout)
    try:
        server.run()
    except KeyboardInterrupt:
        server.shutdown()
        print("")


if __name__ == '__main__':
    run_server()
