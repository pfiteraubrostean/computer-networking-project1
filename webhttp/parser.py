#!/usr/bin/env python3

"""HTTP response and request parsers

This module contains parses for HTTP response and HTTP requests.
"""

from abc import ABCMeta, abstractmethod

from webhttp.message import Request, Response


class MessageParser(metaclass=ABCMeta):
    """Parser for Messages

    Parsing of messages is complicated as the socket buffer can contain
    multiple or partial messages. The parser should be used in the following
    way:

    parser = RequestParser()
    while not parser.is_done():
        buff = ...
        parse.parse_buffer(buff)
    messages = parser.messages

    You will have to implement parse_buffer in MessageParser and is_done and
    parse_start_line in RequestParser and ResponseParser.
    """

    def __init__(self):
        self.messages = []

    def parse_buffer(self, buff):
        """Parse the messages in the buffer

        Args:
            buff (str): the buffer contents received from socket, this may
                        contain multiple or partial messages.
        """
        message = self.empty_message()
        self.messages.append(message)

    @abstractmethod
    def is_done(self):
        """Check if parser is done

        See RFC 7230 section 3.3.3.
        """
        pass

    @abstractmethod
    def empty_message(self):
        """Create an empty message"""
        pass

    @abstractmethod
    def parse_start_line(self, start_line, message):
        """Parse the start line

        Args:
            start_line (str): the start line
            message (Request/Response): the message to be filled
        """
        pass


class RequestParser(MessageParser):
    """Class that parses a HTTP request"""

    def __init__(self):
        """Initialize the RequestParser"""
        super().__init__()

    def is_done(self):
        """Check if parser is done

        See RFC 7230 section 3.3.3.
        """
        return True

    def empty_message(self):
        """Create an empty request"""
        return Request()

    def parse_start_line(self, request_line, request):
        """Parse the request line

        Args:
            request_line (str): the request line
            request (Request): the request to be filled
        """
        request.method = "GET"


class ResponseParser(MessageParser):
    """Class that parses a HTTP response"""

    def __init__(self):
        """Initialize the ResponseParser"""
        super().__init__()

    def is_done(self):
        """Check if parser is done

        See RFC 7230 section 3.3.3.
        """
        pass

    def empty_message(self):
        """Create an empty response"""
        return Response()

    def parse_start_line(self, status_line, response):
        """Parse the status line

        Args:
            status_line (str): the status line
            response (Request): the response to be filled
        """
        response.code = 500
