#!/usr/bin/env python3

"""HTTP Messages

This modules contains classes for representing HTTP responses and requests.
"""

from abc import ABCMeta, abstractmethod


REASON_DICT = {
    # Dictionary for code reasons
    # Format: code : "Reason"
    500 : "Internal Server Error"
}


class Message(metaclass=ABCMeta):
    """Class that stores a HTTP Message"""

    class HeaderName:
        """ Header field name

        Allows case-insensitive comparisons, while still printing
        case-sensitive header field names.
        """

        def __init__(self, name):
            """ Initialize HeaderName

            Args:
                name (str): name of header
            """
            self.name = name

        def __str__(self):
            """ Convert to string

            Usage:
                >>> header_name = HeaderName("Test")
                >>> str(header_name)
                "Test"
                >>> header_name = HeaderName("test")
                >>> str(header_name)
                "test"
            """
            return self.name

        def __eq__(self, other):
            """ Compare two HeaderNames

            Usage:
                >>> HeaderName("Test") == HeaderName("test")
                True
            """
            if isinstance(other, Message.HeaderName):
                return self.name.lower() == other.name.lower()
            return NotImplemented

        def __hash__(self):
            """ Hash, needed for dict """
            return hash(self.name.lower())

    def __init__(self):
        """Initialize the Message"""
        self.version = "HTTP/1.1"
        self.startline = ""
        self.body = ""
        self.headerdict = dict()

    def set_header(self, name, value):
        """Add a header and its value

        Args:
            name (str): name of header
            value (str): value of header
        """
        key = Message.HeaderName(name)
        self.headerdict[key] = value

    def get_header(self, name):
        """Get the value of a header

        Args:
            name (str): name of header

        Returns:
            str: value of header, empty if header does not exist
        """
        key = Message.HeaderName(name)
        if key in self.headerdict:
            return self.headerdict[key]
        else:
            return ""

    def __str__(self):
        """Convert the Message to a string

        Returns:
            str: representation the can be sent over socket
        """
        start_line = self.get_start_line()
        message = ""
        return message

    @abstractmethod
    def get_start_line(self):
        """Get the start line of the message

        Returns:
            str: start line of message
        """
        pass


class Request(Message):
    """Class that stores a HTTP request"""

    def __init__(self):
        """Initialize the Request"""
        super().__init__()
        self.method = ""
        self.uri = ""

    def get_start_line(self):
        """Get the start line of the message

        Returns:
            str: start line of message
        """
        start_line = ""
        return start_line


class Response(Message):
    """Class that stores a HTTP Response"""

    def __init__(self):
        """Initialize the Response"""
        super().__init__()
        self.code = 500

    def get_start_line(self):
        """Get the start line of the message

        Returns:
            str: start line of message
        """
        start_line = ""
        return start_line
