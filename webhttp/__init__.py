#!/usr/bin/env python3

"""HTTP Package

This package contains the following modules:
    * message: Module for HTTP responses/requests
    * composer: Module for composing responses to requests
    * parser: Module for parsing HTTP responses/requests
    * resource: Module for resources (files)
    * server: Module which contains a HTTP server
"""
