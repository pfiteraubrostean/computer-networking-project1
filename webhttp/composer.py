#!/usr/bin/env python3

""" Composer for HTTP responses

This module contains a composer, which can compose responses to
HTTP requests from a client.
"""

from webhttp.message import Request, Response
from webhttp.resource import Resource


class ResponseComposer:
    """Class that composes a HTTP response to a HTTP request"""

    def __init__(self, timeout):
        """Initialize the ResponseComposer

        Args:
            timeout (int): connection timeout
        """
        self.timeout = timeout

    def compose_response(self, request):
        """Compose a response to a request

        Args:
            request (Request): request from client

        Returns:
            Response: response to request
        """
        response = Response()

        # Stub code
        response.code = 200
        response.set_header("Content-Length", 4)
        response.set_header("Connection", "close")
        response.body = "Test"

        return response
